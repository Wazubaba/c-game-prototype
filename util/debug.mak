CC= clang

DEBUGFLAGS= -g -fno-omit-frame-pointer -fsanitize=address
WARNFLAGS= -Wall -Wextra -Werror
OPTIMIZATIONS= -O0
INCLUDE= -Iinclude -Isrc -Iinclude/engine \
				 $(shell sdl2-config --cflags) \
				 $(shell pkg-config --cflags libconfig) \
				 $(shell agar-config --cflags)


CFLAGS= $(OPTIMIZATIONS) $(WARNFLAGS) $(DEBUGFLAGS) $(INCLUDE)

LDFLAGS= -L/usr/local/lib -Wl,-rpath,/usr/local/lib -Wl,--enable-new-dtags \
				 -lpthread -lconfig -lSDL2 -lSDL2_mixer -lSDL2_image -lconfig 

BIN= hordewars

SRC= $(shell find src -type f -name "*.c")

OBJ= $(patsubst %.c, %.o, $(SRC))

.PHONY: all clean

all: $(BIN)

$(BIN): $(OBJ)
	$(CC) $(CFLAGS) -o$(BIN) $^ lib/engine.a $(LDFLAGS) 

clean:
	-$(RM) $(OBJ)
	-$(RM) $(BIN)

