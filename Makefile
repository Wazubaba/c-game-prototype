all:
	@/bin/echo -e "Please specify type of build: debug or release"

release:
	$(MAKE) -f util/release.mak

debug:
	$(MAKE) -f util/debug.mak

clean:
	$(MAKE) -f util/debug.mak clean

