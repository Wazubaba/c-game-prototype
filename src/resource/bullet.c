#include "resource/bullet.h"

struct Bullet* bullet_init(struct Bullet* bullet, SDL_Texture* texture, const int cellWidth, const int cellHeight)
{
	short int isAlloc = 0;

	if (bullet == NULL)
	{
		bullet = malloc(sizeof(struct Bullet));
		if (bullet == NULL)
			return NULL;

		isAlloc = 1;
	}
	
	/* Initialize all values */
	memset(bullet, 0, sizeof(struct Bullet));
	bullet->onHeap = isAlloc;

	/* Calculate the vis from the supplied texture reference */
	vis_init(&bullet->vis, texture, cellWidth, cellHeight);

	return bullet;
}

void bullet_destroy(struct Bullet* bullet)
{
	if (bullet != NULL)
	{
		short int shouldFree = bullet->onHeap;

		memset(bullet, 0, sizeof(struct Bullet));

		if (shouldFree)
			free(bullet);
	}
}

