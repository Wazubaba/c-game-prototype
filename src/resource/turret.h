#ifndef TURRET_H
#define TURRET_H

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include "weapon.h"
#include "visual_instance.h"

struct Turret
{
	char* name;
	int health;
	float rotationSpeed;
	struct Weapon* weapon;

	int xOffset;
	int yOffset;
	float angle;

	int aiMode;
	
	struct Vis vis;

	short int onHeap;
};

struct Turret* turret_init(struct Turret* turret, const char* name, SDL_Texture* texture, const int cellWidth, const int cellHeight);

void turret_destroy(struct Turret* turret);

#endif /* TURRET_H */

