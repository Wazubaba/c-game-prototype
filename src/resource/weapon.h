#ifndef WEAPON_H
#define WEAPON_H

#include <string.h>
#include <SDL.h>
#include <malloc.h>

#include "visual_instance.h"
#include "bullet.h"

struct Weapon
{
	char* name;
	int xOffset;
	int yOffset;
	float angle;

	int fireRate;

	int ammoRemaining;
	int ammoMax;

	struct Bullet* bullet;

	struct Vis vis;

	short int onHeap;
};

struct Weapon* weapon_init(struct Weapon* weapon, const char* name, SDL_Texture* texture, const int cellWidth, const int cellHeight);

void weapon_destroy(struct Weapon* weapon);

#endif /* WEAPON_H */

