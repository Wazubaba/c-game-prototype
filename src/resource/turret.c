#include <resource/turret.h>

struct Turret* turret_init(struct Turret* turret, const char* name, SDL_Texture* texture, const int cellWidth, const int cellHeight)
{
	short int isAlloc = 0;

	if (turret == NULL)
	{
		turret = malloc(sizeof(struct Turret));
		if (turret == NULL)
			return NULL;

		isAlloc = 1;
	}
	
	/* Initialize all values */
	memset(turret, 0, sizeof(struct Turret));
	turret->onHeap = isAlloc;

	turret->name = strdup(name);
	if (turret->name == NULL)
	{
		if (turret->onHeap)
			free(turret);

		return NULL;
	}

	/* Calculate the vis from the supplied texture reference */
	vis_init(&turret->vis, texture, cellWidth, cellHeight);

	return turret;
}

void turret_destroy(struct Turret* turret)
{
	if (turret != NULL)
	{
		short int shouldFree = turret->onHeap;
		free(turret->name);

		memset(turret, 0, sizeof(struct Turret));

		if (shouldFree)
			free(turret);
	}
}

