#include <resource/unit.h>

struct Unit* unit_create(struct Unit* unit, const char* name, SDL_Texture* texture, const int cellWidth, const int cellHeight)
{
	short int isAlloc = 0;

	if (unit == NULL)
	{
		unit = malloc(sizeof(struct Unit));
		if (unit == NULL)
			return NULL;

		isAlloc = 1;
	}

	/* Initialize all values */
	memset(unit, 0, sizeof(struct Unit));
	unit->onHeap = isAlloc;
	
	unit->name = strdup(name);
	if (unit->name == NULL)
	{
		if (unit->onHeap)
			free(unit);

		return NULL;
	}

	/* Calculate the vis from the supplied texture reference */
	vis_init(&unit->vis, texture, cellWidth, cellHeight);

	return unit;
}

void unit_destroy(struct Unit* unit)
{
	if (unit != NULL)
	{
		short int shouldFree = unit->onHeap;
		free(unit->name);

		memset(unit, 0, sizeof(struct Unit));

		if (shouldFree)
			free(unit);
	}
}

