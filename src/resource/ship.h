#ifndef SHIP_H
#define SHIP_H

#include <string.h>
#include <SDL.h>
#include <malloc.h>

#include "visual_instance.h"
#include "weapon.h"
#include "turret.h"
#include "unit.h"

struct Hanger
{
	int xOffset;
	int yOffset;
	float angle;
	struct Unit* unit;
};

struct Ship
{
	char* name;
	int health;

	/** Main and retro thrust speed */
	int thrust;

	/** Strafing speed */
	int strafe;

	/** Rotation speed */
	int yaw;

	/** Array of hangers aboard the ship */
	struct Hanger** hangers;

	/** Array of turrets aboard the ship */
	struct Turret** turrets;

	/** Array of weapons aboard the ship (think like missiles that launch out of the hull)*/
	struct Weapon** weapons;

	/** A ship can only outfit a single spinal weapon */
	struct Weapon* special;

	struct Vis vis;

	short int onHeap;
};

/**
 * Initialize a new ship.
 * @param ship pointer to the ship to intialize
 * @param name name of the ship
 * @param texture reference to the texture of the ship
 * @param cellWidth width of a cell on the spritesheet
 * @param cellHeight height of a cell on the spritesheet
 * @return pointer to the new ship or NULL on failure
*/
struct Ship* ship_init(struct Ship* ship, const char* name, SDL_Texture* texture, const int cellWidth, const int cellHeight);

/**
 * Destroy a ship and free all resources.
 * @param ship pointer to the ship to free
*/
void ship_destroy(struct Ship* ship);

#endif /* SHIP_H */

