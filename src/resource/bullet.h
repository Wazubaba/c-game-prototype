#ifndef BULLET_H
#define BULLET_H

#include <string.h>
#include <SDL.h>
#include <malloc.h>

#include "visual_instance.h"

enum
{
	BULLETTYPE_NORMAL,
	BULLETTYPE_BEAM,
	BULLETTYPE_DRONE,
	BULLETTYPE_HOMING,
};

struct Bullet
{
	float angle;
	int damage;
	int speed;

	int explosionRadius;
	float explosionFalloff;

	int bulletType;

	struct Vis vis;

	short int onHeap;
};

struct Bullet* bullet_init(struct Bullet* bullet, SDL_Texture* texture, const int cellWidth, const int cellHeight);

void bullet_destroy(struct Bullet* bullet);


#endif /* BULLET_H */

