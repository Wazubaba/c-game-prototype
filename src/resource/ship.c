#include <resource/ship.h>

struct Ship* ship_init(struct Ship* ship, const char* name, SDL_Texture* texture, const int cellWidth, const int cellHeight)
{
	short int isAlloc = 0;

	if (ship == NULL)
	{
		ship = malloc(sizeof(struct Ship));
		if (ship == NULL)
			return NULL;

		isAlloc = 1;
	}

	/* Initialize all values */
	memset(ship, 0, sizeof(struct Ship));
	ship->onHeap = isAlloc;

	ship->name = strdup(name);
	if (ship->name == NULL)
	{
		if (ship->onHeap)
			free(ship);

		return NULL;
	}

	/* Calculate the vis from the supplied texture reference */
	vis_init(&ship->vis, texture, cellWidth, cellHeight);

	return ship;
}

void ship_destroy(struct Ship* ship)
{
	if (ship != NULL)
	{
		short int shouldFree = ship->onHeap;
		free(ship->name);

		memset(ship, 0, sizeof(struct Ship));

		if (shouldFree)
			free(ship);
	}
}

