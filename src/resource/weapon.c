#include <resource/weapon.h>

struct Weapon* weapon_init(struct Weapon* weapon, const char* name, SDL_Texture* texture, const int cellWidth, const int cellHeight)
{
	short int isAlloc = 0;

	if (weapon == NULL)
	{
		weapon = malloc(sizeof(struct Weapon));
		if (weapon == NULL)
			return NULL;

		isAlloc = 1;
	}

	/* Initialize all values */
	memset(weapon, 0, sizeof(struct Weapon));
	weapon->onHeap = isAlloc;

	weapon->name = strdup(name);
	if (weapon->name == NULL)
	{
		if (weapon->onHeap)
			free(weapon);

		return NULL;
	}

	/* Calculate the vis from the supplied texture reference */
	vis_init(&weapon->vis, texture, cellWidth, cellHeight);

	return weapon;
}

void weapon_destroy(struct Weapon* weapon)
{
	if (weapon != NULL)
	{
		short int shouldFree = weapon->onHeap;
		free(weapon->name);

		memset(weapon, 0, sizeof(struct Weapon));

		if (shouldFree)
			free(weapon);
	}
}

