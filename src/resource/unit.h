#ifndef UNIT_H
#define UNIT_H

#include <string.h>
#include <SDL.h>
#include <malloc.h>

#include "visual_instance.h"
#include "weapon.h"

struct Unit
{
	/** Name of the class of the unit, i.e. Type 6 Drone */
	char* name;

	/** Level of the unit - used in calculations possibly? */
	int level;

	/** Health */
	int hp;

	/** How fast the unit can move */
	int speed;

	/** Contains a pointer to various weapons data */
	struct Weapon** weapons;

	/** Array of turrets */
	struct Turret** turrets;

	/** Special weapon that causes an animation to be played before, during, and after firing */
	struct Weapon* special;

	/** Graphical context */
	struct Vis vis;

	short int onHeap;
};

/**
 * Create a new unit
 * @param unitref - Storage for the unit to be created
 * @param name - Name of the unit (i.e. Type 6 drone)
 * @param level - Level of the unit
 * @param hp - total hit points of the unit
 * @param speed - how fast the unit can move
 * @param weapon - pointer to the type of weapon the unit has
 * @param texture - pointer to the texture the unit uses
 * @param cellWidth - width of the sprite on the spritesheet
 * @param cellHeight - height of the sprite on the spriteshee
 * @return Pointer to the new unit, or NULL on failure
*/
struct Unit* unit_create(struct Unit* unit, const char* name, SDL_Texture* texture, const int cellWidth, const int cellHeight);

/**
 * Release all alocated memory used by the unit
 * @param unitref Pointer to the unit
*/
void unit_destroy(struct Unit* unitref);

#endif /* UNIT_H */

