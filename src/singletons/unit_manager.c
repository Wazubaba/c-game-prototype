#include <singletons/unit_manager.h>

void unitman_cleanup(void* data)
{
	unit_destroy((struct Unit*) data);
}

struct UnitManager* unitman_init(struct UnitManager* unitman)
{
	unsigned char isAlloc = 0;

	if (unitman == NULL)
	{
		unitman = malloc(sizeof(struct UnitManager));
		if (unitman == NULL)
			return NULL;

		isAlloc = 1;
	}

	if (rmap_init(&unitman->refmap, unitman_cleanup) == NULL)
	{
		if (isAlloc)
			free(unitman);
		return NULL;
	}

	unitman->onHeap = isAlloc;

	return unitman;
}

void unitman_destroy(struct UnitManager* unitman)
{
	if (unitman != NULL)
	{
		unsigned char shouldFree = unitman->onHeap;
		rmap_destroy(&unitman->refmap);

		unitman->onHeap = 0;
		
		if (shouldFree)
			free(unitman);
	}
}

struct Unit* unitman_register(struct UnitManager* unitman, const char* name, struct Unit* unit)
{
	if (unit == NULL)
		return NULL;

	void* rhs = rmap_register(&unitman->refmap, name, (void*) unit);

	if (rhs != NULL)
		slog(2, SLOG_NONE, "[%s] Registered unit '%s'", strclr(CLR_NAGENTA, "LOAD"), name);

	return (struct Unit*) rhs;
}

struct Unit* unitman_lookup(struct UnitManager* unitman, const char* name)
{
	void* rhs = rmap_lookup(&unitman->refmap, name);
	return (struct Unit*) rhs;
}

