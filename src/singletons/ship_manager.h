#ifndef SHIP_MANAGER_H
#define SHIP_MANAGER_H

#include <malloc.h>
#include <slog.h>

#include <refmap.h>

#include "resource/ship.h"

struct ShipManager
{
	struct RefMap refmap;
	unsigned char onHeap;
};

/**
 * Initialize a ship manager with no definitions.
 * @param shipman pointer to the ship manager to use
 * @return pointer to new shipman or NULL on failure
*/
struct ShipManager* shipman_init(struct ShipManager* shipman);

/**
 * Destroy a ship manager and free all of the resources managed by it.
 * @param shipman pointer to the ship manager to use
*/
void shipman_destroy(struct ShipManager* shipman);

/**
 * Register a ship to the manager.
 * @param shipman pointer to the ship manager to use
 * @param ship pointer to the ship to be managed
 * @return pointer to the newly registered ship or NULL on failure
*/
struct Ship* shipman_register(struct ShipManager* shipman, const char* name, struct Ship* ship);

/**
 * Query the manager for a ship specified by name.
 * @param shipman pointer to the ship manager to use
 * @param name name of the ship to query for
 * @return pointer to the found ship or NULL on failure
*/
struct Ship* shipman_lookup(struct ShipManager* shipman, const char* name);

#endif /* SHIP_MANAGER_H */

