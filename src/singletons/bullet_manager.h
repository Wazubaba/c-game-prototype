#ifndef BULLET_MANAGER_H
#define BULLET_MANAGER_H

#include <malloc.h>
#include <slog.h>

#include <refmap.h>

#include "resource/bullet.h"

struct BulletManager
{
	struct RefMap refmap;
	unsigned char onHeap;
};

/**
 * Initialize a bullet manager with no definitions.
 * @param bulletman pointer to the bullet manager to use
 * @return pointer to new bullet manager or NULL on failure
*/
struct BulletManager* bulletman_init(struct BulletManager* bulletman);

/**
 * Destroy a bullet manager and free all of the resources managed by it.
 * @param bulletman pointer to the bullet manager to use
*/
void bulletman_destroy(struct BulletManager* bulletman);

/**
 * Register a bullet to the manager.
 * @param bulletman pointer to the bullet manager to use
 * @param bullet pointer to the bullet to be managed
 * @return pointer to the newly registered bullet or NULL on failure
*/
struct Bullet* bulletman_register(struct BulletManager* bulletman, const char* name, struct Bullet* bullet);

/**
 * Query the manager for a bullet specified by name.
 * @param bulletman pointer to the bullet manager to use
 * @param name name of the bullet to query for
 * @return pointer to the found bullet or NULL on failure
*/
struct Bullet* bulletman_lookup(struct BulletManager* bulletman, const char* name);

#endif /* BULLET_MANAGER_H */

