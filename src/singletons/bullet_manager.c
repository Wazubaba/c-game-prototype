#include <singletons/bullet_manager.h>

void bulletman_cleanup(void* data)
{
	bullet_destroy((struct Bullet*) data);
}

struct BulletManager* bulletman_init(struct BulletManager* bulletman)
{
	unsigned char isAlloc = 0;

	if (bulletman == NULL)
	{
		bulletman = malloc(sizeof(struct BulletManager));
		if (bulletman == NULL)
			return NULL;

		isAlloc = 1;
	}

	if (rmap_init(&bulletman->refmap, bulletman_cleanup) == NULL)
	{
		if (isAlloc)
			free(bulletman);
		return NULL;
	}

	bulletman->onHeap = isAlloc;

	return bulletman;
}

void bulletman_destroy(struct BulletManager* bulletman)
{
	if (bulletman != NULL)
	{
		unsigned char shouldFree = bulletman->onHeap;
		rmap_destroy(&bulletman->refmap);

		bulletman->onHeap = 0;

		if (shouldFree)
			free(bulletman);
	}
}

struct Bullet* bulletman_register(struct BulletManager* bulletman, const char* name, struct Bullet* bullet)
{
	if (bullet == NULL)
		return NULL;

	void* rhs = rmap_register(&bulletman->refmap, name, (void*) bullet);
	
	if (rhs != NULL)
		slog(2, SLOG_NONE, "[%s] Registered bullet '%s'", strclr(CLR_NAGENTA, "LOAD"), name);

	return (struct Bullet*) rhs;
}

struct Bullet* bulletman_lookup(struct BulletManager* bulletman, const char* name)
{
	void* rhs = rmap_lookup(&bulletman->refmap, name);
	return (struct Bullet*) rhs;
}

