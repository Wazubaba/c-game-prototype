#include <singletons/ship_manager.h>

void shipman_cleanup(void* data)
{
	ship_destroy((struct Ship*) data);
}

struct ShipManager* shipman_init(struct ShipManager* shipman)
{
	unsigned char isAlloc = 0;

	if (shipman == NULL)
	{
		shipman = malloc(sizeof(struct ShipManager));
		if (shipman == NULL)
			return NULL;

		isAlloc = 1;
	}

	if (rmap_init(&shipman->refmap, shipman_cleanup) == NULL)
	{
		if (isAlloc)
			free(shipman);
		return NULL;
	}

	shipman->onHeap = isAlloc;

	return shipman;
}

void shipman_destroy(struct ShipManager* shipman)
{
	if (shipman != NULL)
	{
		unsigned char shouldFree = shipman->onHeap;
		rmap_destroy(&shipman->refmap);

		shipman->onHeap = 0;

		if (shouldFree)
			free(shipman);
	}
}

struct Ship* shipman_register(struct ShipManager* shipman, const char* name, struct Ship* ship)
{
	if (ship == NULL)
		return NULL;

	void* rhs = rmap_register(&shipman->refmap, name, (void*) ship);

	if (rhs != NULL)
		slog(2, SLOG_NONE, "[%s] Registered ship '%s'", strclr(CLR_NAGENTA, "LOAD"), name);

	return (struct Ship*) rhs;
}

struct Ship* shipman_lookup(struct ShipManager* shipman, const char* name)
{
	void* rhs = rmap_lookup(&shipman->refmap, name);
	return (struct Ship*) rhs;
}

