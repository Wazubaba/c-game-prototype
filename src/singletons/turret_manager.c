#include <singletons/turret_manager.h>

void turretman_cleanup(void* data)
{
	turret_destroy((struct Turret*) data);
}

struct TurretManager* turretman_init(struct TurretManager* turretman)
{
	unsigned char isAlloc = 0;

	if (turretman == NULL)
	{
		turretman = malloc(sizeof(struct TurretManager));
		if (turretman == NULL)
			return NULL;

		isAlloc = 1;
	}
	
	if (rmap_init(&turretman->refmap, turretman_cleanup) == NULL)
	{
		if (isAlloc)
			free(turretman);
		return NULL;
	}

	turretman->onHeap = isAlloc;

	return turretman;
}

void turretman_destroy(struct TurretManager* turretman)
{
	if (turretman != NULL)
	{
		unsigned char shouldFree = turretman->onHeap;
		rmap_destroy(&turretman->refmap);

		turretman->onHeap = 0;

		if (shouldFree)
			free(turretman);
	}
}

struct Turret* turretman_register(struct TurretManager* turretman, const char* name, struct Turret* turret)
{
	if (turret == NULL)
		return NULL;

	void* rhs = rmap_register(&turretman->refmap, name, (void*) turret);

	if (rhs != NULL)
		slog(2, SLOG_NONE, "[%s] Registered turret '%s'", strclr(CLR_NAGENTA, "LOAD"), name);

	return (struct Turret*) rhs;
}

struct Turret* turretman_lookup(struct TurretManager* turretman, const char* name)
{
	void* rhs = rmap_lookup(&turretman->refmap, name);
	return (struct Turret*) rhs;
}

