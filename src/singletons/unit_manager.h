#ifndef UNIT_MANAGER_H
#define UNIT_MANAGER_H

#include <malloc.h>
#include <slog.h>

#include <refmap.h>

#include "resource/unit.h"

struct UnitManager
{
	struct RefMap refmap;
	unsigned char onHeap;
};

/**
 * Initialize a unit manager with no definitions.
 * @param unitman pointer to the unit manager to use
 * @return pointer to the new unit manager or NULL on failure
*/
struct UnitManager* unitman_init(struct UnitManager* unitman);

/**
 * Destroy a unit manager and free all of the resources managed by it.
 * @param unitman pointer to the unit manager to use
*/
void unitman_destroy(struct UnitManager* unitman);

/**
 * Register a unit to the manager.
 * @param unitman pointer to the unit manager to use
 * @param unit pointer to the unit to be managed
 * @return pointer to the newly registered unit or NULL on failure
*/
struct Unit* unitman_register(struct UnitManager* unitman, const char* name, struct Unit* unit);

/**
 * Query the manager for a unit specified by name.
 * @param unitman pointer to the unit manager to use
 * @param name name of the unit to query for
 * @return pointer to the found unit or NULL on failure
*/
struct Unit* unitman_lookup(struct UnitManager* unitman, const char* name);

#endif /* UNIT_MANAGER_H */

