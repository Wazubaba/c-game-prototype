#ifndef Weapon_MANAGER_H
#define Weapon_MANAGER_H

#include <malloc.h>
#include <slog.h>

#include <refmap.h>

#include "resource/weapon.h"

struct WeaponManager
{
	struct RefMap refmap;
	unsigned char onHeap;
};

/**
 * Initialize a weapon manager with no definitions.
 * @param weaponman pointer to the weapon manager to use
 * @return pointer to the new weapon manager or NULL on failure
*/
struct WeaponManager* weaponman_init(struct WeaponManager* weaponman);

/**
 * Destroy a weapon manager and free all of the resources managed by it.
 * @param weaponman pointer to the weapon manager to use
*/
void weaponman_destroy(struct WeaponManager* weaponman);

/**
 * Register a weapon to the manager.
 * @param weaponman pointer to the weapon manager to use
 * @param weapon pointer to the weapon to be managed
 * @return pointer to the newly registered weapon or NULL on failure
*/
struct Weapon* weaponman_register(struct WeaponManager* weaponman, const char* name, struct Weapon* weapon);

/**
 * Query the manager for a weapon specified by name.
 * @param weaponman pointer to the weapon manager to use
 * @param name name of the weapon to query for
 * @return pointer to the found weapon or NULL on failure
*/
struct Weapon* weaponman_lookup(struct WeaponManager* weaponman, const char* name);

#endif /* Weapon_MANAGER_H */

