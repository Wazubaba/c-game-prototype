#include <singletons/weapon_manager.h>

void weaponman_cleanup(void* data)
{
	weapon_destroy((struct Weapon*) data);
}

struct WeaponManager* weaponman_init(struct WeaponManager* weaponman)
{
	unsigned char isAlloc = 0;

	if (weaponman == NULL)
	{
		weaponman = malloc(sizeof(struct WeaponManager));
		if (weaponman == NULL)
			return NULL;

		isAlloc = 1;
	}

	if (rmap_init(&weaponman->refmap, weaponman_cleanup) == NULL)
	{
		if (isAlloc)
			free(weaponman);
		return NULL;
	}

	weaponman->onHeap = isAlloc;

	return weaponman;
}

void weaponman_destroy(struct WeaponManager* weaponman)
{
	if (weaponman != NULL)
	{
		unsigned char shouldFree = weaponman->onHeap;
		rmap_destroy(&weaponman->refmap);

		weaponman->onHeap = 0;

		if (shouldFree)
			free(weaponman);
	}
}

struct Weapon* weaponman_register(struct WeaponManager* weaponman, const char* name, struct Weapon* weapon)
{
	if (weapon == NULL)
		return NULL;

	void* rhs = rmap_register(&weaponman->refmap, name, (void*) weapon);

	if (rhs != NULL)
		slog(2, SLOG_NONE, "[%s] Registered weapon '%s'", strclr(CLR_NAGENTA, "LOAD"), name);

	return (struct Weapon*) rhs;
}

struct Weapon* weaponman_lookup(struct WeaponManager* weaponman, const char* name)
{
	void* rhs = rmap_lookup(&weaponman->refmap, name);
	return (struct Weapon*) rhs;
}

