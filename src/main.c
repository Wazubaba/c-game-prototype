#define SDL_MAIN_HANDLED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <SDL.h>
#include <slog.h>

#include <engine/engine.h>
#include <engine/scene_manager.h>
#include <engine/visual_instance.h>

int main(void)
{
	SDL_SetMainReady();

	struct EngineInfo engine;
	if (engine_init(&engine) == NULL)
	{
		slog(0, SLOG_FATAL, "Failed to initialise engine, shutting down");
		return EXIT_FAILURE;
	}
	/*

	int w,h;
	SDL_Texture* tex = txm_lookup(&engine.tex_m, "test");
	if (tex == NULL)
	{
		slog(0, SLOG_FATAL, "Failed to load texture :(");
		engine_shutdown(&engine);
		return EXIT_FAILURE;
	}

	SDL_QueryTexture(tex, NULL, NULL, &w, &h);
	printf("Got dimensions: %dx%d\n", w, h);

	struct Unit unit;
	if (unit_create(&unit, "test-unit", tex, w, h) == NULL)
	{
		fprintf(stderr, "Failed to initialise unit, shutting down");
		engine_shutdown(&engine);
		return EXIT_FAILURE;
	}

	struct Unit animationTest;
	if (unit_create(&animationTest, "animation-test", txm_lookup(&engine.tex_m, "animation-test"), 16, 16) == NULL)
	{
		unit_destroy(&unit);
		fprintf(stderr, "Failed to initialize animation test unit, shutting down");
		engine_shutdown(&engine);
		return EXIT_FAILURE;
	}

	vis_init_animations(&animationTest.vis, 10, 0);

	SDL_QueryTexture(unit.vis.texture, NULL, NULL, &w, &h);
	printf("Unit internal texture reference dimsize: %dx%d\n", w, h);


	struct SceneManager scm;
	scm_init(&scm);

	if (scm_register(&scm, &unit.vis) == NULL)
	{
		puts("failed to register entity to scene manager :(");
		engine_shutdown(&engine);
		return EXIT_FAILURE;
	}
	if (scm_register(&scm, &animationTest.vis) == NULL)
	{
		puts("failed to register animation test to scene manager :(");
		engine_shutdown(&engine);
		return EXIT_FAILURE;
	}
	slog(0, SLOG_NONE, "Initialized scene manager");

	int i;
	for (i = 0; i < 5; ++i)
	{
		scm_draw(&scm, engine.renderer);
		vis_calcAnimation(&animationTest.vis);

		// Take a quick break after all that hard work
		SDL_Delay(500);
	}
*/
	/* Engine Shutdown - Free All Of The Things! */
/*
	unit_destroy(&animationTest);
	unit_destroy(&unit);
	slog(0, SLOG_NONE, "Deleted test unit");

	scm_destroy(&scm);
	slog(0, SLOG_NONE, "Destroyed test scene manager");
*/
	engine_shutdown(&engine);

	return 0;
}

